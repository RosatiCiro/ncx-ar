//
//  ViewController.swift
//  DetectionProva
//
//  Created by Ciro Rosati on 25/03/2020.
//  Copyright © 2020 Ciro Rosati. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate, UIImagePickerControllerDelegate & UINavigationControllerDelegate {

    @IBOutlet var sceneView: ARSCNView!
    var grids = [Grid]()
    var imageView = UIImageView()
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            // Set the view's delegate
            sceneView.delegate = self
            
            // Show statistics such as fps and timing information
            sceneView.showsStatistics = false
            sceneView.debugOptions = ARSCNDebugOptions.showFeaturePoints
            
            // Create a new scene
            let scene = SCNScene()

            // Set the scene to the view
            sceneView.scene = scene
            
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
            sceneView.addGestureRecognizer(gestureRecognizer)
            
            let pinchGestureRecognizer = UIPinchGestureRecognizer (target: self, action: #selector(pinched))
            sceneView.addGestureRecognizer(pinchGestureRecognizer)
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            
            // Create a session configuration
            let configuration = ARWorldTrackingConfiguration()
            configuration.planeDetection = .vertical

            // Run the view's session
            sceneView.session.run(configuration)
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            
            // Pause the view's session
            sceneView.session.pause()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Release any cached data, images, etc that aren't in use.
        }

        // MARK: - ARSCNViewDelegate
        
    /*
        // Override to create and configure nodes for anchors added to the view's session.
        func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
            let node = SCNNode()
         
            return node
        }
    */
        
        func session(_ session: ARSession, didFailWithError error: Error) {
            // Present an error message to the user
            
        }
        
        func sessionWasInterrupted(_ session: ARSession) {
            // Inform the user that the session has been interrupted, for example, by presenting an overlay
            
        }
        
        func sessionInterruptionEnded(_ session: ARSession) {
            // Reset tracking and/or remove existing anchors if consistent tracking is required
            
        }
        
        func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
            guard let planeAnchor = anchor as? ARPlaneAnchor, planeAnchor.alignment == .vertical else { return }
            let grid = Grid(anchor: planeAnchor)
            self.grids.append(grid)
            node.addChildNode(grid)
        }
        
        func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
            guard let planeAnchor = anchor as? ARPlaneAnchor, planeAnchor.alignment == .vertical else { return }
            let grid = self.grids.filter { grid in
                return grid.anchor.identifier == planeAnchor.identifier
                }.first
            
            guard let foundGrid = grid else {
                return
            }
            
            foundGrid.update(anchor: planeAnchor)
        }
        
        @objc func tapped(gesture: UITapGestureRecognizer) {
            // Get 2D position of touch event on screen
            let touchPosition = gesture.location(in: sceneView)
            
            // Translate those 2D points to 3D points using hitTest (existing plane)
            let hitTestResults = sceneView.hitTest(touchPosition, types: .existingPlaneUsingExtent)
            
            // Get hitTest results and ensure that the hitTest corresponds to a grid that has been placed on a wall
            guard let hitTest = hitTestResults.first, let anchor = hitTest.anchor as? ARPlaneAnchor, let gridIndex = grids.index(where: { $0.anchor == anchor }) else {
                return
            }
            addPainting(hitTest, grids[gridIndex])
        }
    
    @objc func pinched(gesture: UIPinchGestureRecognizer){
        
        if gesture.state == .changed{
            
            guard let sceneView = gesture.view as? ARSCNView else{
                return
            }
            let touch = gesture.location(in: sceneView)
            
            let hitTestResults = self.sceneView.hitTest(touch, options: nil)
            if let hitTest = hitTestResults.first{
                let imageNode = hitTest.node
                let pinchScaleX = Float(gesture.scale) * imageNode.scale.x
                let pinchScaleY = Float(gesture.scale) * imageNode.scale.y
                let pinchScaleZ = Float(gesture.scale) * imageNode.scale.z
                imageNode.scale = SCNVector3(pinchScaleX, pinchScaleY, pinchScaleZ)
                
                gesture.scale = 1
            }
        }
    }
        
       
    
    @IBAction func importImage(_ sender: Any) {
        let image = UIImagePickerController()
        image.delegate = self
        image.sourceType = .photoLibrary
        image.allowsEditing = false
        self.present(image, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController,
            didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

            //var newImage: UIImage
            //if let possibleImage = info[.editedImage] as? UIImage {
            //    newImage = possibleImage
            //} else if let possibleImage = info[.originalImage] as? UIImage {
            //    newImage = possibleImage
            //} else {
            //    return
            //}

            if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                imageView.contentMode = .scaleAspectFit
                imageView.image = pickedImage
            }

            dismiss(animated: true, completion: nil)
        }

        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
    func addPainting(_ hitResult: ARHitTestResult, _ grid: Grid) {
               // 1.
               let planeGeometry = SCNPlane(width: 0.2, height: 0.35)
               let material = SCNMaterial()
        material.diffuse.contents = imageView.image
               planeGeometry.materials = [material]
               
               // 2.
               let paintingNode = SCNNode(geometry: planeGeometry)
               paintingNode.transform = SCNMatrix4(hitResult.anchor!.transform)
               paintingNode.eulerAngles = SCNVector3(paintingNode.eulerAngles.x + (-Float.pi / 2), paintingNode.eulerAngles.y, paintingNode.eulerAngles.z)
               paintingNode.position = SCNVector3(hitResult.worldTransform.columns.3.x, hitResult.worldTransform.columns.3.y, hitResult.worldTransform.columns.3.z)
               
               sceneView.scene.rootNode.addChildNode(paintingNode)
               grid.removeFromParentNode()
           }
    
}

