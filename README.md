Technologies used: UIKit, ARkit

In this project we work for the first time with augmented reality. 
We talk about AR and its utility in an Medium article. https://medium.com/apple-developer-academy-federico-ii/let-ar-enter-in-your-daily-things-871512b998d7

During this challenge, I deepened my knowledge about the ARKit framework, even because it was my first experience with the Augmented Reality. About the article, I wrote the "Structure" section. I spent a lot of time creating and developing this little example of AR.